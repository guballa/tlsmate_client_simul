Changelog
#########


v1.0.1 (2022-03-27)
===================

Bugfix
------

* In some cases the key exchange information was not shown due to session resumption  (#5)


v1.0.0-rc1 (2021-12-01)
=======================

* Initial release
