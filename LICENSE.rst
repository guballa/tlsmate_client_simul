The code of this SW is published under the MIT License. The License
is available `here <LICENSE.mit>`_.

In addition this plugin relies on the `client database from SSLLABS
<https://api.ssllabs.com/api/v3/getClients>`_, which is available in
`this directory here <https://gitlab.com/guballa/tlsmate_client_simul/-/blob/master/tlsmate_client_simul/data/getClients.json>`_.
This file is provided by SSLLABS under the `Creative Commons Attribution 3.0
License <https://creativecommons.org/licenses/by/3.0/us/>`_.
